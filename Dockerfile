FROM centos:7

MAINTAINER JungJungIn "call518@gmail.com"

RUN yum -y install epel-release

RUN yum -y install gcc git libxslt-devel openssl-devel

RUN yum -y install libffi-devel python-devel python-pip python-virtualenv

RUN pip install --upgrade pip

RUN git clone https://github.com/openstack/tempest.git

RUN cd tempest && python setup.py install

RUN pip install -r /tempest/requirements.txt

RUN pip install -r /tempest/test-requirements.txt

RUN pip install nose

RUN mkdir /tempest/lock-path/ /tempest/files/

COPY cirros-0.3.5-x86_64-disk.img /tempest/files/cirros-0.3.5-x86_64-disk.img

COPY tempest.conf /tempest/etc/tempest.conf.sample

COPY run_tempest.sh /tempest/run_tempest.sh

COPY run_tests.sh /tempest/run_tests.sh

#RUN cd /tempest && tempest init cloud-001

#RUN cp /tempest/etc/tempest.conf.sample /tempest/cloud-001/etc/tempest.conf

#RUN cd /tempest/ && tempest run --workspace cloud-001

WORKDIR /tempest
